package hu.kovacs.gergo.tprsapp.model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;

/**
 * The class equivalent to an entry in the .csv files
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.02.27.
 */
@Data
@Entity(tableName = "entry_table")
public class Entry {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "uid")
    private long mUID;

    @ColumnInfo(name = "english")
    private String mEnglish;

    @ColumnInfo(name = "hungarian")
    private String mHungarian;

    @ColumnInfo(name = "original")
    private String mOriginal;

    @ColumnInfo(name = "hiragana")
    private String mHiragana;

    @ColumnInfo(name = "wordtype")
    private String mWordType;

    @ColumnInfo(name = "example")
    private String mExample;

    @ColumnInfo(name = "kanji")
    private String mKanji;

    @ColumnInfo(name = "labels")
    private String mLabels;

    @Nullable
    @Embedded (prefix = "srs_")
    private SRSData mSRSData;

    /**
     * Instantiates a new Entry.
     *
     * @param english   the english
     * @param hungarian the hungarian
     * @param original  the original
     * @param hiragana  the hiragana
     * @param wordType  the word type
     * @param example   the example
     * @param kanji     the kanji
     * @param labels    the labels
     */
    public Entry(String english, String hungarian, String original, String hiragana,
                String wordType, String example, String kanji, String labels, @Nullable SRSData sRSData) {
        this.mEnglish = english;
        this.mHungarian = hungarian;
        this.mOriginal = original;
        this.mHiragana = hiragana;
        this.mWordType = wordType;
        this.mExample = example;
        this.mKanji = kanji;
        this.mLabels = labels;
        this.mSRSData = sRSData;
    }

    @Override
    public String toString(){
        String data = "{" + mEnglish + ", "
                        + mHungarian + ", "
                        + mOriginal + ", "
                        + mHiragana + ", "
                        + mWordType + ", "
                        + mExample + ", "
                        + mKanji + ", "
                        + mLabels;


            if(mSRSData == null){
                return data + ", null}";
            }

            return data + ", " + mSRSData.toString() + "}";
    }

}
