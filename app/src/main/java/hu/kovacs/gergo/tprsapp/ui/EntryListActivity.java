package hu.kovacs.gergo.tprsapp.ui;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hu.kovacs.gergo.tprsapp.R;
import hu.kovacs.gergo.tprsapp.Repository;
import hu.kovacs.gergo.tprsapp.model.Entry;
import hu.kovacs.gergo.tprsapp.util.DebugLoggingHelper;
import hu.kovacs.gergo.tprsapp.view.EntryViewModel;
import hu.kovacs.gergo.tprsapp.view.adapter.EntryListAdapter;

/**
 * The entry list activity of the application. It list all entries that are currently stored in the
 * database
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.02.27.
 */
public class EntryListActivity extends AppCompatActivity {

    /**
     * The simple name of the class. Used for log.*() calls.
     */
    private final String APP = EntryListActivity.class.getSimpleName();

    /**
     * The ViewModel that handles the data display.
     */
    private EntryViewModel mEntryViewModel;

    /**
     * The repository that is resposible for providing access to the data.
     */
    private Repository mRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.all_entries_activity);

        // Setting up our custom toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Setting up the recylerView
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final EntryListAdapter adapter = new EntryListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Setting up the ViewModel so that it listens to the getAllEntries() method
        mEntryViewModel = ViewModelProviders.of(this).get(EntryViewModel.class);
        mEntryViewModel.getAllEntries().observe(this, new Observer<List<Entry>>() {
            @Override
            public void onChanged(@Nullable final List<Entry> words) {
                // Update the cached copy of the words in the adapter.
                adapter.setEntries(words);
            }
        });


        // Getting all of the entries in the repository
        mRepository = mEntryViewModel.getMRepository();
        List<Entry> entries = mRepository.getAllWordsUnsyncronized();

        // logging both the entries and the original source file
        DebugLoggingHelper.logEntriesToConsole(entries);
        DebugLoggingHelper.logFileToConsole(R.string.japanese_words_n5);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
