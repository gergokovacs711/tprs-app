package hu.kovacs.gergo.tprsapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import hu.kovacs.gergo.tprsapp.R;
import hu.kovacs.gergo.tprsapp.view.adapter.MainMenuAdapter;

/**
 * The main menu activity of the application. It is the launcher activity and it serves as a central
 * starting point for the application
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.03.17.
 */

public class MainMenu extends AppCompatActivity {

    /**
     * The listview for the menu points
     */
    private ListView listView;

    /**
     * The Main menu adapter.
     */
    private MainMenuAdapter mainMenuAdapter;

    /**
     * The on create method of the activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_activity);

        listView = findViewById(R.id.main_listview);
        setupMainMenu();
    }

    private void setupMainMenu() {
        // accessing the title and description data. Storing these in string arrays is more..
        // flexible than storing them individually
        String[] title = getResources().getStringArray(R.array.main_menu_list);
        String[] descriptions = getResources().getStringArray(R.array.main_menu_list_description);

        // creating the adapter and connecting it to the listview
        mainMenuAdapter = new MainMenuAdapter(this, title, descriptions);
        listView.setAdapter(mainMenuAdapter);

        // setting up onclick listener for the each menu points
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position) {
                    case 0: {
                        Intent intent = new Intent(MainMenu.this, EntryListActivity.class);
                        startActivity(intent);
                        break;

                    }
                    case 1: {
                        Intent intent = new Intent(MainMenu.this, JsonTestActivity.class);
                        startActivity(intent);
                        break;
                    }
                }
            }
        });
    }
}
