package hu.kovacs.gergo.tprsapp.model.dict

import androidx.room.*
import com.google.gson.annotations.SerializedName
import hu.kovacs.gergo.tprsapp.database.converter.JMDictConverters
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.Kanji
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.Reading
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.Sense

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.04.
 */

/** POJO class based on the JMDict project's .xml file documentation. The project uses JSON files..
 * that have parsed from the origal .xml file. Although the data structure is mostly based on the..
 * .xml files, some changes have been made to accommodate to the current JSON implementation.
 *
 * |XML|
 * The documentation has been copied from the JMDict_e.xml file which is available at:
 * http://www.edrdg.org/jmdict/edict_doc.html (last checked 2019.04.04)
 *
 * |JSON|
 * The .xml has been converted to JSON using Saurabh's JMdict-Parser
 * github repo: https://github.com/tkshnwesper/JMdict-Parser last checked(2019.04.06)
 *
 * NOTE: Some of the class atributes are represented as arrays in the JSON file even though they..
 * only have a single data equivalent in the .xml file
 *
 * entry (ent_seq, k_ele*, r_ele+, sense+)
 *
 * Entries consist of kanji elements, reading elements,
 * general information and sense elements. Each entry must have at
 * least one reading element and one sense element. Others are optional.
 */

@Entity(tableName = "dictionary_entry", indices = [Index(value = ["uid"])])
@TypeConverters(JMDictConverters::class)
data class DictionaryEntry(
        /**
         * ent_seq (#PCDATA)
         *
         * A unique numeric sequence number for each entry
         */
        @PrimaryKey
        @ColumnInfo(name = "uid")
        @SerializedName("ent_seq")
        val sequence: Array<Int>,

        /**
         * k_ele (keb, ke_inf*, ke_pri*)
         *
         * The kanji element, or in its absence, the reading element, is
         * the defining component of each entry.
         * The overwhelming majority of entries will have a single kanji
         * element associated with a word in Japanese. Where there are
         * multiple kanji elements within an entry, they will be orthographical
         * variants of the same word, either using variations in okurigana, or
         * alternative and equivalent kanji. Common "mis-spellings" may be
         * included, provided they are associated with appropriate information
         * fields. Synonyms are not included; they may be indicated in the
         * cross-reference field associated with the sense element.
         */
        @ColumnInfo(name = "kanji_ele")
        @SerializedName("k_ele")
        val kanjiElement: Array<Kanji>?,

        /**
         * r_ele (reb, re_nokanji?, re_restr*, re_inf*, re_pri*)
         *
         * The reading element typically contains the valid readings
         * of the word(s) in the kanji element using modern kanadzukai.
         * Where there are multiple reading elements, they will typically be
         * alternative readings of the kanji element. In the absence of a
         * kanji element, i.e. in the case of a word or phrase written
         * entirely in kana, these elements will define the entry.
         */
        @ColumnInfo(name = "reading_ele")
        @SerializedName("r_ele")
        val readingElement: Array<Reading>,

        /**
         *  sense (stagk*, stagr*, pos*, xref*, ant*, field*, misc*, s_inf*, lsource*, dial*, gloss*)
         *
         *  The sense element will record the translational equivalent
         * of the Japanese word, plus other related information. Where there
         * are several distinctly different meanings of the word, multiple
         * sense elements will be employed.
         */
        @ColumnInfo(name = "sense_ele")
        @SerializedName("sense")
        val sense: Array<Sense>
) {
    /**
     * Auto-generated equals method
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DictionaryEntry

        if (!sequence.contentEquals(other.sequence)) return false
        if (kanjiElement != null) {
            if (other.kanjiElement == null) return false
            if (!kanjiElement.contentEquals(other.kanjiElement)) return false
        } else if (other.kanjiElement != null) return false
        if (!readingElement.contentEquals(other.readingElement)) return false
        if (!sense.contentEquals(other.sense)) return false

        return true
    }

    /**
     * Auto-generated hashCode method
     */
    override fun hashCode(): Int {
        var result = sequence.contentHashCode()
        result = 31 * result + (kanjiElement?.contentHashCode() ?: 0)
        result = 31 * result + readingElement.contentHashCode()
        result = 31 * result + sense.contentHashCode()
        return result
    }
}





