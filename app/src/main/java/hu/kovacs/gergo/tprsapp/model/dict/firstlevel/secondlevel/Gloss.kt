package hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel

import com.google.gson.annotations.SerializedName
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.thirdlevel.GlossAttr

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.06.
 */

/**
 * gloss (#PCDATA | pri)*
 *
 * Within each sense will be one or more "glosses", i.e.
 * target-language words or phrases which are equivalents to the
 * Japanese word. This element would normally be present, however it
 * may be omitted in entries which are purely for a cross-reference.
 */
data class Gloss(
        /**
         * gloss (#PCDATA | pri)
         */
        @SerializedName("_")
        var data: String?,

        /**
         * gloss xml:lang CDATA "eng"
         * gloss g_gend CDATA #IMPLIED
         * gloss g_type CDATA #IMPLIED
         */
        @SerializedName("$")
        var attributes: GlossAttr?

)

