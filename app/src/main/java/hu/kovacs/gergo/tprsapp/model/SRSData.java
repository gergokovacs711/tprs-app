package hu.kovacs.gergo.tprsapp.model;

import java.util.Date;

import lombok.Data;

/**
 * @author Gergo Kovacs
 * @version 1.0
 * @date 2019.03.13.
 */

@Data
public class SRSData {

    private Date mDueDate;
    private Date mStartDate;
    private short mProgress;

    public SRSData(Date dueDate, Date startDate, short progress) {
        mDueDate = dueDate;
        mStartDate = startDate;
        mProgress = progress;
    }

    @Override
    public String toString() {
        return "{" + mDueDate.toString() + ", " + mStartDate.toString() + ", " + mProgress + "}";
    }
}
