package hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel

import com.google.gson.annotations.SerializedName
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.thirdlevel.LanguageSourceAttr

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.06.
 */


/**
 * lsource (#PCDATA)
 *
 * This element records the information about the source
language(s) of a loan-word/gairaigo. If the source language is other
than English, the language is indicated by the xml:lang attribute.
The element value (if any) is the source word or phrase.
 */
data class LanguageSource(
        /**
         * lsource (#PCDATA)
         */
        @SerializedName("_")
        val data: String?,

        /**
         * lsource xml:lang CDATA "eng"
         * lsource ls_type CDATA #IMPLIED
         * lsource ls_wasei CDATA #IMPLIED
         */
        @SerializedName("$")
        val attributes: LanguageSourceAttr


)

