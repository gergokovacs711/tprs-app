package hu.kovacs.gergo.tprsapp.util.json

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import hu.kovacs.gergo.tprsapp.model.dict.DictionaryEntry
import hu.kovacs.gergo.tprsapp.util.ApplicationHelper
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.charset.Charset

/**
 * The JsonHandler class is used to parse JSON objects into Objects using Gson. JSON can be parsed..
 * as streams or in one go. Large files should only be parsed as streams.
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.03.
 */

object JsonHandler {
    /**
     * Loads data from an asset file
     * @param fileName the name of the file located in the asset folder
     * @param characterSet the characterset of the given file (ex: UTF-8)
     * @return - the string opened from the asset OR null if there was a problem during loading the..
     * file
     */
    fun loadDataFromAsset(fileName: String, characterSet: Charset = Charset.defaultCharset()): String {
        var asset: String = ""
        try {
            val inputStream = ApplicationHelper.context.assets.open(fileName)
            val size = inputStream.available()
            val buffer = ByteArray(size)

            inputStream.read(buffer)
            inputStream.close()

            asset = String(buffer, characterSet)

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return asset
    }

    /**
     * Opens an asset file as an InputStream
     *
     * @param file the name of the file located in the assets folder
     * @return the InputStream that is created from the file OR null
     */
    fun openFileAsInputStream(file: String) : InputStream? {
        val inputStream: InputStream? = null
        try {
            return ApplicationHelper.context.assets.open(file)
        }
         catch (e: IOException) {
             e.printStackTrace()
        }
        return inputStream
    }

    /**
     * Parses a JSON file into a List using Gson
     *
     * @param T the type of the object which are to parsed from the file
     * @param fileName the name of the file located in the assets folder
     * @param gson the Gson object which is used to parse the JSON
     */
    inline fun <reified T> gsonDeserializer(fileName: String, gson: Gson): List<T> {
        val json = loadDataFromAsset(fileName)
        val dataListType = object : TypeToken<Collection<T>>() {}.type
        return gson.fromJson(json, dataListType)
    }

    /**
     * Loads a file from the assets folder and deserializes it. Calls lamdaMethod on each T object.
     *
     * @param T the type of the object which are to parsed from the file
     * @param fileName the name of the file located in the assets folder
     * @param gson the Gson object which is used to parse the JSON
     * @param batchSize the number of T instances that the lambdaMethod should be performed on in..
     *                  one go
     * @param lambdaMethod the method that is called on every T object right after it is created
     */
    inline fun <reified T> gsonStreamDeserializer(fileName: String,
                                                  gson: Gson,
                                                  batchSize: Int = 1,
                                                  lambdaMethod: (thing: List<T>) -> Any = { println(it) }){
        // argument checking
        require(batchSize > 0) {
            "batchSize cannot be smaller than 1"
        }

        val inputStream = openFileAsInputStream(fileName)
        val jsonReader = JsonReader(InputStreamReader(inputStream, Charset.defaultCharset()))

        var thing: T
        val thingArray = mutableListOf<T>()

        jsonReader.beginArray()
        while (jsonReader.hasNext()) {
            thing = gson.fromJson(jsonReader, T::class.java)
            thingArray.add(thing)

            if(!jsonReader.hasNext() || thingArray.size == batchSize){
                lambdaMethod(thingArray.toMutableList())
                thingArray.clear()
            }
        }
        jsonReader.close()
    }
}

