package hu.kovacs.gergo.tprsapp.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.kovacs.gergo.tprsapp.R
import hu.kovacs.gergo.tprsapp.model.dict.DictionaryEntry
import kotlinx.android.synthetic.main.jmdict_item.view.*

/**
 * List adapter for the RecycleView that holds the DictionaryEntries
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.30.
 */
class DictionaryEntryListAdapter(val appContext: Context) : RecyclerView.Adapter<DictionaryEntryListAdapter.ViewHolder>() {
    /**
     * The data that is temporarily cached inside the adapter
     */
    private var entries: List<DictionaryEntry>? = null

    /**
     * The inner ViewHolder class that holds one item from the list
     */
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: DictionaryEntry?) {
            with(itemView) {
                // TODO: change placeholder text
                entry_text_english.text = item?.readingElement?.get(0)?.base?.get(0) ?: if(item == null) "item is null" else "item is not null"
                entry_text_original.text = item?.sense?.get(0)?.stagKanji?.get(0) ?: if(item == null) "item is null" else "item is not null"
            }
        }
    }

    /**
     *  ViewHolder onCreate implementation.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return A new ViewHolder that holds a View of DictionaryEntry
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(appContext)
        val view = inflater.inflate(R.layout.jmdict_item, parent, false)
        return ViewHolder(view)
    }

    /**
     *  getItemCount implementation.
     *
     *  @return The size of entries
     */
    override fun getItemCount(): Int = entries?.size ?: 0

    /**
     * onBindViewHolder implementation.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(entries?.get(position))

    /**
     *  This method is used to update entries. It also notifies any registered observers that the..
     *  data set has changed via notifyDataSetChanged().
     */
    fun setEntries(entries: List<DictionaryEntry>) {
        this.entries = entries
        notifyDataSetChanged()
    }
}

