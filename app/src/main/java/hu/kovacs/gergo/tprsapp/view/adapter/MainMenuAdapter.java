package hu.kovacs.gergo.tprsapp.view.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import hu.kovacs.gergo.tprsapp.R;
import hu.kovacs.gergo.tprsapp.util.ApplicationHelper;

/**
 * @author Gergo Kovacs
 * @version 1.0
 * @date 2019.03.17.
 */


public class MainMenuAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private TextView title;
    private TextView description;
    private String[] titleArray;
    private String[] descriptionArray;
    private ImageView imageView;

    public MainMenuAdapter(Context context, String[] title, String[] description) {
        this.context = context;
        titleArray = title;
        descriptionArray = description;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return titleArray.length;
    }

    @Override
    public Object getItem(int position) {

        return titleArray[position];
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view == null) {
            view = layoutInflater.inflate(R.layout.main_menu_item, null);

        }

        title = (TextView) view.findViewById(R.id.main_item_title);
        description = (TextView) view.findViewById(R.id.main_item_description);
        imageView = (ImageView) view.findViewById(R.id.main_item_image);

        title.setText(titleArray[position]);
        description.setText(descriptionArray[position]);

        // setting menu icon from local image array
        TypedArray imageArray =
                ApplicationHelper.Companion.getContext().getResources().obtainTypedArray(
                        R.array.main_menu_icon_images);
        imageView.setImageResource(imageArray.getResourceId(position, -1));

        imageArray.recycle();

        return view;
    }
}
