package hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.thirdlevel

import com.google.gson.annotations.SerializedName

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.06.
 */

data class GlossAttr(
        /**
         * gloss xml:lang CDATA "eng"
         *
         * The xml:lang attribute defines the target language of the
         * gloss. It will be coded using the three-letter language code from
         * the ISO 639 standard. When absent, the value "eng" (i.e. English)
         * is the default value.
         */
        @SerializedName("xml:lang")
        var language: String = "eng",

        /**
         * gloss g_gend CDATA #IMPLIED
         *
         * The g_gend attribute defines the gender of the gloss (typically
         * a noun in the target language. When absent, the gender is either
         * not relevant or has yet to be provided.
         */
        @SerializedName("g_gend")
        var gender: String?,

        /**
         * gloss g_type CDATA #IMPLIED
         *
         * The g_type attribute specifies that the gloss is of a particular
         * type, e.g. "lit" (literal), "fig" (figurative), "expl" (explanation).
         */
        @SerializedName("g_type")
        var type: String?


)

