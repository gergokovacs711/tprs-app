package hu.kovacs.gergo.tprsapp.database

import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.GsonBuilder
import hu.kovacs.gergo.tprsapp.R
import hu.kovacs.gergo.tprsapp.model.dict.DictionaryEntry
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.Gloss
import hu.kovacs.gergo.tprsapp.util.ApplicationHelper
import hu.kovacs.gergo.tprsapp.util.json.GlossTypeAdapter
import hu.kovacs.gergo.tprsapp.util.json.JsonHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * SQLite based, Room database for the application. It uses the database file provided by the JMDict..
 * project
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.20.
 */
@Database(entities = [DictionaryEntry::class], version = 1, exportSchema = false)
abstract class JMDictDatabase : RoomDatabase() {
    companion object {
        /**
         *  The name of the class. Used for Log.*() calls
         */
        val appTag: String = JMDictDatabase::class.java.simpleName

        /**
         * The singleton instance of the database. by lazy guaranties that it is also thread-safe
         */
        val instance: JMDictDatabase by lazy {
            Room.databaseBuilder(ApplicationHelper.context, JMDictDatabase::class.java, "jmdict_database")
                    // adding the main callback for onCreate and onOpen methods
                    .addCallback(roomDatabaseCallback)
                    // allowing DB wiping on version update
                    .fallbackToDestructiveMigration()
                    .build()
        }

        /**
         *  Function for populating the database. It launches a main coroutine on the GlobalScope..
         *  which parses through the JMDict_e.json database file, and spawns child coroutines that..
         *  insert the parsed data into the database
         */
        private fun populateDataBase() {
            GlobalScope.launch {
                val dictionaryFileName = ApplicationHelper.context.getString(R.string.JMDict_e)
                val gson = GsonBuilder().registerTypeAdapter(Gloss::class.java, GlossTypeAdapter()).create()
                val batchSize = 1000
                val dao = instance.jmDictDao()

                // this function parses through @dictionaryFileName and creates @DictionaryEntry objects..
                // if the number of created objects reach @batchSize, it launches the coroutine defined..
                // in the lambda expression. For more detail check the function's documentation
                JsonHandler.gsonStreamDeserializer<DictionaryEntry>(dictionaryFileName, gson, batchSize) {
                    launch(Dispatchers.IO) {
                        dao.insertList(it)
                    }
                }
            }
        }

        /**
         * Callback function for populating the database
         */
        val roomDatabaseCallback = object : RoomDatabase.Callback() {

            /**
             * On the creation of the database it gets populated on another thread
             * @param database the SQLite database instance on which the callback is performed
             */
            override fun onCreate(database: SupportSQLiteDatabase) {
                super.onCreate(database)
                Log.i(appTag, "Database onCreate")
                populateDataBase()
            }

            /**
             * On open of the database.
             * @param database the SQLite database instance on which the callback is performed
             */
            override fun onOpen(database: SupportSQLiteDatabase) {
                super.onOpen(database)
                Log.i(appTag, "Database onOpen")
            }
        }
    }

    /**
     *  This method returns a JMDictDao object which is used to access the database
     *  It has to declared abstract, Room itself will create its implementation
     */
    abstract fun jmDictDao(): JMDictDao

}