package hu.kovacs.gergo.tprsapp.database.converter;

import androidx.room.TypeConverter;

import java.util.Date;

/**
 * Converter between Long and Date types. SQLite cannot store complex types that it does not know
 * how to handle, therefore a converter must be used for any unkown complex types.
 *
 * @author Gergo Kovacs
 * @version 1.0
 * @date 2019.03.13.
 */
public class DateConverter {
    /**
     * Creates a new Date object from a time stamp. If timeStamp is null than the return value is
     * null as
     * well.
     *
     * @param timeStamp the time stamp
     * @return the date
     */
    @TypeConverter
    public static Date timeStampToDate(Long timeStamp) {
        return timeStamp == null ? null : new Date(timeStamp);
    }

    /**
     * Creates a new Long object from a date. If date is null than the return value is null as
     * well.
     *
     * @param date the date
     * @return the long
     */
    @TypeConverter
    public static Long dateToTimeStamp(Date date) {
        return date == null ? null : date.getTime();
    }


}
