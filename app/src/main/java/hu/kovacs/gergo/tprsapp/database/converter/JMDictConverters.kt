package hu.kovacs.gergo.tprsapp.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.Kanji
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.Reading
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.Sense


/**
 * Converters for DictionaryEntry class. Since Room doesn't support Lists natively, they need..
 * converters which creates JSONs from the them.
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.26.
 */

object JMDictConverters {
    private val gson = Gson()

    @TypeConverter
    @JvmStatic
    fun toSequence(sequence: Int): Array<Int> {
        return arrayOf(sequence)
    }

    @TypeConverter
    @JvmStatic
    fun toKanjiElement(kanjiElement: String): Array<Kanji>? {
        val listType = object : TypeToken<Array<Kanji>?>() {}.type
        return gson.fromJson(kanjiElement, listType)
    }

    @TypeConverter
    @JvmStatic
    fun toReadingElement(readingElement: String): Array<Reading> {
        val listType = object : TypeToken<Array<Reading>>() {}.type
        return gson.fromJson(readingElement, listType)
    }

    @TypeConverter
    @JvmStatic
    fun toSenseElement(sense: String): Array<Sense> {
        val listType = object : TypeToken<Array<Sense>>() {}.type
        return gson.fromJson(sense, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromSequence(sequence: Array<Int>): Int = sequence[0]

    @TypeConverter
    @JvmStatic
    fun fromKanjiElement(kanjiElement: Array<Kanji>?): String = gson.toJson(kanjiElement)

    @TypeConverter
    @JvmStatic
    fun fromRadingElement(readingElement: Array<Reading>): String = gson.toJson(readingElement)

    @TypeConverter
    @JvmStatic
    fun fromSenseElement(sense: Array<Sense>): String = gson.toJson(sense)
}

