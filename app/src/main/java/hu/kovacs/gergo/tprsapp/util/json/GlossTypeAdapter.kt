package hu.kovacs.gergo.tprsapp.util.json

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.Gloss
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.thirdlevel.GlossAttr

/**
 * Custom type adapter for the Gloss class.
 * @see Gloss
 * @see GlossAttr
 *
 * The gloss data can be contain special attributes and in that case it is represented as an object..
 * in the JSON. Example:
 * "gloss": [
 *  {
 * "_": "kanji iteration mark",
 * "$": {
 * "g_type": "expl"
 *   }
 *  }
 * ]
 *
 * But if the there is no corresponding attribute data for the gloss data, it is represented as a..
 * list element. Example:
 * "gloss": [
 * "maru mark",
 * "semivoiced sound",
 * "p-sound"
 * ]
 *
 * Because of this mixed representation this class must be used during JSON parsing
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.06.
 */

class GlossTypeAdapter : TypeAdapter<Gloss>() {
    /**
     * TypeAdapter's write method. Currently unimplemented, since there is no need to write the..
     * database source file.
     */
    override fun write(out: JsonWriter?, value: Gloss?) {}

    /**
     * TypeAdapter's read method. This method does the custom parsing of the Gloss object
     *
     * @param reader the JsonReader that is used to parse the file
     * @return the parsed Gloss object, which may or may not have a GlossAttr object
     */
    override fun read(reader: JsonReader?): Gloss {
        // creates an empty gloss for parsing, if the reader is null we still return the Gloss object
        val gloss = Gloss(null, null)

        if (reader != null) {
            val jsonToken = reader.peek()

            when {
                // if there is only string data for the gloss, then we parse it and leave the..
                // attributes null
                jsonToken.equals(JsonToken.STRING) -> gloss.data = reader.nextString()

                // but if there are attributes then the data and the attributes are within an object..
                // see class documentation for more details
                jsonToken.equals(JsonToken.BEGIN_OBJECT) -> {
                    reader.beginObject()

                    while (!reader.peek().equals(JsonToken.END_OBJECT)) {
                        when {
                            // if we find a name we parse it
                            reader.peek().equals(JsonToken.NAME) -> {
                                val nextName = reader.nextName()

                                when {
                                    // this is the data of Gloss
                                    nextName.equals("_") ->
                                        gloss.data = reader.nextString()

                                    // these are the attributes that make up GlossAttr
                                    nextName.equals("$") -> {
                                        reader.beginObject()
                                        gloss.attributes = GlossAttr(gender = null, type = null)

                                        // we parse all GlossAtrr data
                                        while (!reader.peek().equals(JsonToken.END_OBJECT)) {
                                            val nextAttrName = reader.nextName()
                                            when {
                                                nextAttrName.equals("xml:lang") ->
                                                    gloss.attributes?.language = reader.nextString()

                                                nextAttrName.equals("g_gend") ->
                                                    gloss.attributes?.gender = reader.nextString()

                                                nextAttrName.equals("g_type") ->
                                                    gloss.attributes?.type = reader.nextString()
                                            }
                                        }
                                        reader.endObject()
                                    }
                                }
                            }
                        }
                    }
                    reader.endObject()
                }
            }
        }
        return gloss
    }
}

