package hu.kovacs.gergo.tprsapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import hu.kovacs.gergo.tprsapp.R
import hu.kovacs.gergo.tprsapp.databinding.ActivityJsonTestBinding
import hu.kovacs.gergo.tprsapp.model.dict.DictionaryEntry
import hu.kovacs.gergo.tprsapp.view.DictEntryViewModel
import hu.kovacs.gergo.tprsapp.view.adapter.DictionaryEntryListAdapter

/**
 * Test activity for JSON testing purposes
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.04.07.
 */

class JsonTestActivity : AppCompatActivity() {
    lateinit var entryViewModel: DictEntryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_json_test)
        val layoutBinding = DataBindingUtil.setContentView<ActivityJsonTestBinding>(this, R.layout.activity_json_test)

        val entryAdapter = DictionaryEntryListAdapter(this)
        val recyclerView = layoutBinding.jsonTestRecyclerView
        recyclerView.adapter = entryAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        entryViewModel = ViewModelProviders.of(this).get(DictEntryViewModel::class.java)
        entryViewModel.dictEntries.observe(this, Observer<List<DictionaryEntry>>(entryAdapter::setEntries))
    }

}
