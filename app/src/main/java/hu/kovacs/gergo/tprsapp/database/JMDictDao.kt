package hu.kovacs.gergo.tprsapp.database

import androidx.paging.DataSource
import androidx.room.*
import hu.kovacs.gergo.tprsapp.model.dict.DictionaryEntry

/**
 * The interface for data queries in the database
 * It provides an easier way to access the SQLite database
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.20.
 */

@Dao
interface JMDictDao {
    /**
     * Inserts a single entry into the database
     *
     * @param entry the entry to insert
     */
    @Insert
    fun insert(entry: DictionaryEntry)

    /**
     * Deletes a single entry from the database
     *
     * @param entry the entry to delete
     */
    @Delete
    fun delete(entry: DictionaryEntry)

    /**
     * Deletes all entries from the database
     */
    @Query("DELETE FROM dictionary_entry")
    fun deleteAll()

    /**
     * Insert the list of entries into the database
     *
     * @param entries the list of entries to insert
     */
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertList(entries: List<DictionaryEntry>)

    /**
     * Returns all of the entries from the database
     *
     * @return every entry from the database
     */
    @Query("SELECT * FROM dictionary_entry")
    fun getAllEntries() : List<DictionaryEntry>

    /**
     * Return a Paging Factory, with the given amount of DictionaryEntry elements
     */
    @Query("SELECT * FROM dictionary_entry ORDER BY sense_ele DESC")
    fun getAllEntriesPager() : DataSource.Factory<Int, DictionaryEntry>

    /**
     * Return an entry that matches the uid that is given as the parameter
     *
     * @param uid the uid of the entry which is to searched for
     * @return the list of entries that match the uid
     */
    @Query("SELECT * FROM dictionary_entry WHERE uid = :uid")
    fun getEntryByUID(uid: String) : List<DictionaryEntry>

    /**
     * Query for counting the number of entries in the database
     *
     * @return the number of entries located in the database
     */
    @Query("SELECT COUNT(*) FROM dictionary_entry")
    fun getSizeOfDB() : Int

}

