package hu.kovacs.gergo.tprsapp.model.dict.firstlevel

import com.google.gson.annotations.SerializedName
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.Gloss
import hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.LanguageSource

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.04.
 */

/**
 * sense (stagk*, stagr*, pos*, xref*, ant*, field*, misc*, s_inf*, lsource*, dial*, gloss*)
 * The sense element will record the translational equivalent
 * of the Japanese word, plus other related information. Where there
 * are several distinctly different meanings of the word, multiple
 * sense elements will be employed.
 */
data class Sense(
        /**
         * stagk (#PCDATA)
         * stagr (#PCDATA)
         *
         * These elements, if present, indicate that the sense is restricted
         * to the lexeme represented by the keb and/or reb.
         */
        @SerializedName("stagk")
        val stagKanji: Array<String>?,
        @SerializedName("stagr")
        val stagReading: Array<String>?,

        /**
         * xref (#PCDATA)
         *
         * This element is used to indicate a cross-reference to another
         * entry with a similar or related meaning or sense. The content of
         * this element is typically a keb or reb element in another entry. In some
         * cases a keb will be followed by a reb and/or a sense number to provide
         * a precise target for the cross-reference. Where this happens, a JIS
         * "centre-dot" (0x2126) is placed between the components of the
         * cross-reference. The target keb or reb must not contain a centre-dot.
         */
        @SerializedName("xref")
        val crossReference: Array<String>?,

        /**
         * ant (#PCDATA)
         *
         * This element is used to indicate another entry which is an
         * antonym of the current entry/sense. The content of this element
         * must exactly match that of a keb or reb element in another entry.
         */
        @SerializedName("ant")
        val antonym: Array<String>?,

        /**
         * pos (#PCDATA)
         *
         * Part-of-speech information about the entry/sense. Should use
         * appropriate entity codes. In general where there are multiple senses
         * in an entry, the part-of-speech of an earlier sense will apply to
         * later senses unless there is a new part-of-speech indicated.
         */
        @SerializedName("pos")
        val partOfSpeech: Array<String>?,


        /**
         * field (#PCDATA)
         *
         * Information about the field of application of the entry/sense.
         * When absent, general application is implied. Entity coding for
         * specific fields of application.
         *
         */
        @SerializedName("field")
        val fieldOfApplication: Array<String>?,

        /**
         * misc (#PCDATA)
         *
         * This element is used for other relevant information about
         * the entry/sense. As with part-of-speech, information will usually
         * apply to several senses.
         */
        @SerializedName("misc")
        val miscellaneous: Array<String>?,

        /**
         * lsource (#PCDATA)
         *
         * This element records the information about the source
         * language(s) of a loan-word/gairaigo. If the source language is other
         * than English, the language is indicated by the xml:lang attribute.
         * The element value (if any) is the source word or phrase.
         *
         */
        @SerializedName("lsource")
        val languageSources: Array<LanguageSource>?,

        /**
         * dial (#PCDATA)
         *
         * For words specifically associated with regional dialects in
         * Japanese, the entity code for that dialect, e.g. ksb for Kansaiben.
         */
        @SerializedName("dial")
        val dialect: Array<String>?,

        /**
         * gloss (#PCDATA | pri)
         *
         * Within each sense will be one or more "glosses", i.e.
         * target-language words or phrases which are equivalents to the
         * Japanese word. This element would normally be present, however it
         * may be omitted in entries which are purely for a cross-reference.
         *
         */
        @SerializedName("gloss")
        val glosses: Array<Gloss>?,

        /**
         * s_inf (#PCDATA)
         *
         * The sense-information elements provided for additional
         * information to be recorded about a sense. Typical usage would
         * be to indicate such things as level of currency of a sense, the
         * regional variations, etc.
         */
        @SerializedName("s_inf")
        val senseInformation: Array<String>?
) {

    /**
     * Auto-generated equals method
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Sense

        if (stagKanji != null) {
            if (other.stagKanji == null) return false
            if (!stagKanji.contentEquals(other.stagKanji)) return false
        } else if (other.stagKanji != null) return false
        if (stagReading != null) {
            if (other.stagReading == null) return false
            if (!stagReading.contentEquals(other.stagReading)) return false
        } else if (other.stagReading != null) return false
        if (crossReference != null) {
            if (other.crossReference == null) return false
            if (!crossReference.contentEquals(other.crossReference)) return false
        } else if (other.crossReference != null) return false
        if (antonym != null) {
            if (other.antonym == null) return false
            if (!antonym.contentEquals(other.antonym)) return false
        } else if (other.antonym != null) return false
        if (partOfSpeech != null) {
            if (other.partOfSpeech == null) return false
            if (!partOfSpeech.contentEquals(other.partOfSpeech)) return false
        } else if (other.partOfSpeech != null) return false
        if (fieldOfApplication != null) {
            if (other.fieldOfApplication == null) return false
            if (!fieldOfApplication.contentEquals(other.fieldOfApplication)) return false
        } else if (other.fieldOfApplication != null) return false
        if (miscellaneous != null) {
            if (other.miscellaneous == null) return false
            if (!miscellaneous.contentEquals(other.miscellaneous)) return false
        } else if (other.miscellaneous != null) return false
        if (languageSources != null) {
            if (other.languageSources == null) return false
            if (!languageSources.contentEquals(other.languageSources)) return false
        } else if (other.languageSources != null) return false
        if (dialect != null) {
            if (other.dialect == null) return false
            if (!dialect.contentEquals(other.dialect)) return false
        } else if (other.dialect != null) return false
        if (glosses != null) {
            if (other.glosses == null) return false
            if (!glosses.contentEquals(other.glosses)) return false
        } else if (other.glosses != null) return false
        if (senseInformation != null) {
            if (other.senseInformation == null) return false
            if (!senseInformation.contentEquals(other.senseInformation)) return false
        } else if (other.senseInformation != null) return false

        return true
    }


    /**
     * Auto-generated hashCode method
     */
    override fun hashCode(): Int {
        var result = stagKanji?.contentHashCode() ?: 0
        result = 31 * result + (stagReading?.contentHashCode() ?: 0)
        result = 31 * result + (crossReference?.contentHashCode() ?: 0)
        result = 31 * result + (antonym?.contentHashCode() ?: 0)
        result = 31 * result + (partOfSpeech?.contentHashCode() ?: 0)
        result = 31 * result + (fieldOfApplication?.contentHashCode() ?: 0)
        result = 31 * result + (miscellaneous?.contentHashCode() ?: 0)
        result = 31 * result + (languageSources?.contentHashCode() ?: 0)
        result = 31 * result + (dialect?.contentHashCode() ?: 0)
        result = 31 * result + (glosses?.contentHashCode() ?: 0)
        result = 31 * result + (senseInformation?.contentHashCode() ?: 0)
        return result
    }
}

