package hu.kovacs.gergo.tprsapp.util

import android.app.Application
import android.content.Context

/**
 * Provides and access to an Application instance. It is used to acquire resources outside of
 * activity classes
 *
 * @author Gergo Kovacs
 * @version 1.0
 * @date 2019.03.02.
 */


class ApplicationHelper : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    companion object {
        private var INSTANCE: ApplicationHelper? = null

        /**
         * Get context
         *
         * @return the context
         */
        val context: Context
            get() = INSTANCE!!.applicationContext
    }
}
