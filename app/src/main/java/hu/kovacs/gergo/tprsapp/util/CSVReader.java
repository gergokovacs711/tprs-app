package hu.kovacs.gergo.tprsapp.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hu.kovacs.gergo.tprsapp.model.Entry;

/**
 * The purpose of this class is to read and parse a .csv file that is provides with the application
 *
 * @author Gergo Kovacs
 * @version 1.0
 * @date 2019.03.02.
 */


public class CSVReader {
    /**
     * Acquires and opens the file as an InputStream then passes it to parseStreamIntoEntries() for
     * parsing
     *
     * @param idOfTheStringResource the id of the string resource (eg R.string.my_resource)
     * @return the list of entries
     */
    public static List<Entry> loadAndParseFile(int idOfTheStringResource) {

        // Opening the String to the filePath
        Context context = ApplicationHelper.Companion.getContext();
        String filePath = context.getString(idOfTheStringResource);


        // opening and parsing the file
        List<Entry> entries = null;
        try (InputStream csvInputStream = context.getAssets().open(filePath)) {
            entries = parseStreamIntoEntries(csvInputStream);

        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return entries;
    }

    /**
     * Parses the inputStream into entries than returns them as a list.
     *
     * @param inputStream the input stream of the file to be parsed
     * @return the list of entries
     * @throws IOException the io exception
     */
    private static List<Entry> parseStreamIntoEntries(InputStream inputStream) throws IOException {

        String nextReadLine = "";
        String separatorChar = ":";

        try (BufferedReader buffReader = new BufferedReader(new InputStreamReader(inputStream))) {
            List<Entry> objectList = new ArrayList<>();
            List<String> entry;

            while ((nextReadLine = buffReader.readLine()) != null) {

                entry = Arrays.asList(nextReadLine.split(separatorChar));

                objectList.add(new Entry(entry.get(0), entry.get(1), entry.get(2), entry.get(3),
                                         entry.get(4), entry.get(5), entry.get(6), entry.get(7), null));
            }

            return objectList;

        } catch (IOException exception) {
            throw new IOException("Error during parsing the input stream");
        }
    }
}