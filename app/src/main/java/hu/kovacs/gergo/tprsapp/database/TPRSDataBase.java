package hu.kovacs.gergo.tprsapp.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import hu.kovacs.gergo.tprsapp.R;
import hu.kovacs.gergo.tprsapp.database.converter.DateConverter;
import hu.kovacs.gergo.tprsapp.model.Entry;
import hu.kovacs.gergo.tprsapp.model.SRSData;
import hu.kovacs.gergo.tprsapp.util.CSVReader;

/**
 * The SQLite database for the application
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.02.27.
 */
@Database(entities = {Entry.class}, version = 2, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class TPRSDataBase extends RoomDatabase {

    /**
     * The simple name of the class. Used for log.*() calls.
     */
    private final static String APP_TAG = TPRSDataBase.class.getSimpleName();

    /**
     * The thread safe instance of the database
     */
    private static volatile TPRSDataBase INSTANCE;

    /**
     * Returns and RoomDao. This method has to be abstract. The Room component will handle this on
     * its own.
     *
     * @return the entry dao
     */
    public abstract RoomDao getRoomDao();


    /**
     * Returns the instance of the database. If the database has no been created, it is
     * instantiated first.
     *
     * @param context the context
     * @return the data base
     */
    public static TPRSDataBase getDataBase(final Context context) {
        // if there is no DB yet then it creates one
        if (INSTANCE == null) {

            // synchronizing onto the class to avoid thread collision
            synchronized (TPRSDataBase.class) {

                // if the DB still haven't benn created by any thread then it creates it
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                                    TPRSDataBase.class,
                                                    "tprs_database")
                            // adding the main callback for onCreate and onOpen methods
                            .addCallback(roomDatabaseCallback)
                            // allowing DB wiping on version update
                            .fallbackToDestructiveMigration()
                            .build();

                }
            }
        }

        // returning the only instance of the DB
        return INSTANCE;
    }

    /**
     * Callback function for the database
     */
    private static RoomDatabase.Callback roomDatabaseCallback = new RoomDatabase.Callback() {

        /**
         * On the creation of the database it gets populated on another thread
         * @param database the SQLite database instance on which the callback is performed
         */
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase database) {
            super.onCreate(database);
            Log.i(APP_TAG, "Database onCreate");
            new PopulateDBAsync(INSTANCE).execute();
        }


        /**
         * On open of the database.
         * @param database the SQLite database instance on which the callback is performed
         */
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase database) {
            super.onOpen(database);
            Log.i(APP_TAG, "Database onOpen");
        }

    };

    /**
     * An AsyncTask for populating the database
     */
    private static class PopulateDBAsync extends AsyncTask<Void, Void, Void> {
        /**
         * Dao from the database
         */
        private final RoomDao roomDao;

        /**
         * Instantiates a new PopulateDBAsync.
         *
         * @param database the database
         */
        PopulateDBAsync(TPRSDataBase database) {
            roomDao = database.getRoomDao();
        }

        /**
         * Loads entries from the file into entries then inserts them into the database
         */
        @Override
        protected Void doInBackground(Void... voids) {

            // loading entries from the given file
            List<Entry> entries = CSVReader.loadAndParseFile(R.string.japanese_words_n5);

            // generating mock data
            List<SRSData> srsDataArray = generateMockSRSData(entries.size());
            Iterator<SRSData> iterator = srsDataArray.iterator();

            // inserting entries into the DB with the included mock SRSData
            for (Entry entry : entries) {
                entry.setMSRSData(iterator.next());
                roomDao.insert(entry);
            }
            return null;
        }

        /**
         * Generates numberOfMocKData amount of mock SRSData
         *
         * @param numberOfMockData the amount of mock data that is to be generated
         */
        private List<SRSData> generateMockSRSData(int numberOfMockData) {
            // getting the current date and time in millis
            Calendar calendar = Calendar.getInstance();
            long currentTimeInMillis = calendar.getTime().getTime();

            ArrayList<SRSData> srsDataArrayList = new ArrayList<>();

            for (int i = 0; i < numberOfMockData; i++) {
                short progress = (short) (Math.random() * 10);

                // 90000000 millis equal to 25 hours, we offset the time for the 2 dates so that
                // we can see a difference between them
                currentTimeInMillis += 90000000;
                srsDataArrayList.add(new SRSData(new Date(currentTimeInMillis - 90000000),
                                                 new Date(currentTimeInMillis), progress));
            }
            return srsDataArrayList;
        }
    }
}
