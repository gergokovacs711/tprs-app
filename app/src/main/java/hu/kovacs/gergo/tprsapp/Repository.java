package hu.kovacs.gergo.tprsapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;

import hu.kovacs.gergo.tprsapp.database.TPRSDataBase;
import hu.kovacs.gergo.tprsapp.model.Entry;
import hu.kovacs.gergo.tprsapp.database.RoomDao;

/**
 * The Repository provides access to the database.
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.02.27.
 */


public class Repository {

    /**
     * The Data Access Object (DAO) provides an interface for database queries
     */
    private RoomDao mRoomDao;

    /**
     * The List of entries which are updated by the LiveData component. It is more efficient to
     * store here than to always get it from the database
     */
    private LiveData<List<Entry>> mAllWords;

    /**
     * Instantiates a new Repository.
     *
     * @param application the application
     */
    public Repository(Application application) {
        TPRSDataBase database = TPRSDataBase.getDataBase(application);
        mRoomDao = database.getRoomDao();
        mAllWords = mRoomDao.getAllEntries();
    }

    /**
     * Returns all the entries in a synchronized, LiveData<> type which ensures that whenever there
     * is a change in one or more of the entries, the list will be updated to the live data.
     *
     * @return the synchronized data
     */
    public LiveData<List<Entry>> getAllWords() {
        return mAllWords;
    }


    /**
     * Inserts a single entry into the database
     *
     * @param entry the entry to insert
     */
    public void insert(Entry entry) {
        new insertAsyncTask(mRoomDao).execute(entry);

    }

    /**
     * Gets all the entries, but they will not be updated if the database changes
     *
     * @return the list of entries
     */
    public List<Entry> getAllWordsUnsyncronized() {
        try {
            return new getAllAsyncTask(mRoomDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * AsyncTask class for multithreaded database calls. If database execution were allowed on the
     * main thread the UI may freeze for the time it takes the database query to finish.
     */
    private static class insertAsyncTask extends AsyncTask<Entry, Void, Void> {
        /**
         * Dao from the outer class
         */
        private RoomDao mWordDao;

        /**
         * Instantiates a new Insert async task.
         *
         * @param dao the dao
         */
        insertAsyncTask(RoomDao dao) {
            mWordDao = dao;
        }

        @Override
        protected Void doInBackground(Entry... entries) {
            mWordDao.insert(entries[0]);
            return null;
        }
    }

    /**
     * AsyncTask class for multithreaded database calls. If database execution were allowed on the
     * main thread the UI may freeze for the time it takes the database query to finish.
     */
    private static class getAllAsyncTask extends AsyncTask<Void, Void, List<Entry>> {
        /**
         * Dao from the outer class
         */
        private RoomDao mWordDao;

        /**
         * Instantiates a new Get all async task.
         *
         * @param dao the dao
         */
        getAllAsyncTask(RoomDao dao) {
            mWordDao = dao;
        }

        @Override
        protected List<Entry> doInBackground(Void... voids) {
            return mWordDao.getAllEntriesUnsynchronized();
        }
    }

}
