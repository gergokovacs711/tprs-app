package hu.kovacs.gergo.tprsapp.view

import androidx.lifecycle.ViewModel
import hu.kovacs.gergo.tprsapp.DictRepository

/**
 * ViewModel for DictionaryEntries
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.30.
 */


class DictEntryViewModel : ViewModel() {
    val dictEntries = DictRepository.dictionaryEntries
}
