package hu.kovacs.gergo.tprsapp.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import hu.kovacs.gergo.tprsapp.model.Entry;

/**
 * The interface for data queries in the database
 * It provides an easier way to access the SQLite database
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.02.27.
 */

@Dao
public interface RoomDao {
    /**
     * Inserts a single entry into the database
     *
     * @param entry the entry to insert
     */
    @Insert
    void insert(Entry entry);

    /**
     * Deletes a single entry in the database
     *
     * @param entry the entry to delete
     */
    @Delete
    void delete(Entry entry);

    /**
     * Deletes all entries in the database
     */
    @Query("DELETE FROM entry_table")
    void deleteAll();

    /**
     * Returns all entries in wrapped in a LiveData holder class
     *
     * @return all the entries, synchronized
     */
    @Query("SELECT * FROM entry_table ORDER BY english ASC")
    LiveData<List<Entry>> getAllEntries();

    /**
     * Returns a copy of all entries. They will not be updated upon change in the database.
     *
     * @return all the entries, unsynchronized
     */
    @Query("SELECT * FROM entry_table ORDER BY english ASC")
    List<Entry> getAllEntriesUnsynchronized();
}
