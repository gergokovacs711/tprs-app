package hu.kovacs.gergo.tprsapp.model.dict.firstlevel

import com.google.gson.annotations.SerializedName

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.04.
 */


/**
 * r_ele (reb, re_nokanji?, re_restr*, re_inf*, re_pri*)
 *
 * The reading element typically contains the valid readings
 * of the word(s) in the kanji element using modern kanadzukai.
 * Where there are multiple reading elements, they will typically be
 * alternative readings of the kanji element. In the absence of a
 * kanji element, i.e. in the case of a word or phrase written
 * entirely in kana, these elements will define the entry.
 */
data class Reading(
        /**
         * reb (#PCDATA)
         *
         * this element content is restricted to kana and related
         * characters such as chouon and kurikaeshi. Kana usage will be
         * consistent between the keb and reb elements; e.g. if the keb
         * contains katakana, so too will the reb.
         */
        @SerializedName("reb")
        val base: Array<String>,

        /**
         * re_nokanji (#PCDATA)
         *
         * This element, which will usually have a null value, indicates
         * that the reb, while associated with the keb, cannot be regarded
         * as a true reading of the kanji. It is typically used for words
         * such as foreign place names, gairaigo which can be in kanji or
         * katakana, etc.
         */
        @SerializedName("re_nokanji")
        val noKanji: Array<String>?,

        /**
         * re_restr (#PCDATA)
         *
         * This element is used to indicate when the reading only applies
         * to a subset of the keb elements in the entry. In its absence, all
         * readings apply to all kanji elements. The contents of this element
         * must exactly match those of one of the keb elements.
         */
        @SerializedName("re_restr")
        val readingString: Array<String>?,

        /**
         * re_inf (#PCDATA)
         *
         * General coded information pertaining to the specific reading.
         * Typically it will be used to indicate some unusual aspect of
         * the reading.
         */
        @SerializedName("re_inf")
        val info: Array<String>?,

        /**
         * re_pri (#PCDATA)
         *
         * See the comment on ke_pri above.
         */
        @SerializedName("re_pri")
        val priority: Array<String>?
) {
    /**
     * Auto-generated equals method
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Reading

        if (!base.contentEquals(other.base)) return false
        if (noKanji != other.noKanji) return false
        if (readingString != null) {
            if (other.readingString == null) return false
            if (!readingString.contentEquals(other.readingString)) return false
        } else if (other.readingString != null) return false
        if (info != null) {
            if (other.info == null) return false
            if (!info.contentEquals(other.info)) return false
        } else if (other.info != null) return false
        if (priority != null) {
            if (other.priority == null) return false
            if (!priority.contentEquals(other.priority)) return false
        } else if (other.priority != null) return false

        return true
    }

    /**
     * Auto-generated hashCode method
     */
    override fun hashCode(): Int {
        var result = base.hashCode()
        result = 31 * result + (noKanji?.hashCode() ?: 0)
        result = 31 * result + (readingString?.contentHashCode() ?: 0)
        result = 31 * result + (info?.contentHashCode() ?: 0)
        result = 31 * result + (priority?.contentHashCode() ?: 0)
        return result
    }
}

