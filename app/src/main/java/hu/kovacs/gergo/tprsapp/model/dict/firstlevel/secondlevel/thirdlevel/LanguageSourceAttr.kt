package hu.kovacs.gergo.tprsapp.model.dict.firstlevel.secondlevel.thirdlevel

import com.google.gson.annotations.SerializedName

/**
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.06.
 */

data class LanguageSourceAttr(
        /**
         * lsource xml:lang CDATA "eng"
         *
         * The xml:lang attribute defines the language(s) from which
         * a loanword is drawn.  It will be coded using the three-letter language
         * code from the ISO 639-2 standard. When absent, the value "eng" (i.e.
         * English) is the default value. The bibliographic (B) codes are used.
         */
        @SerializedName("xml:lang")
        var language: String = "eng",

        /**
         * lsource ls_type CDATA #IMPLIED
         *
         * The ls_type attribute indicates whether the lsource element
         * fully or partially describes the source word or phrase of the
         * loanword. If absent, it will have the implied value of "full".
         * Otherwise it will contain "part".
         */
        @SerializedName("ls_type")
        var type: String?,

        /**
         * lsource ls_wasei CDATA #IMPLIED
         *
         * The ls_wasei attribute indicates that the Japanese word
         * has been constructed from words in the source language, and
         * not from an actual phrase in that language. Most commonly used to
         * indicate "waseieigo".
         */
        @SerializedName("ls_wasei")
        var wasei: String?
)

