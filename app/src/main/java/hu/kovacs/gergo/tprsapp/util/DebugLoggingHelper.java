package hu.kovacs.gergo.tprsapp.util;

import android.util.Log;

import java.util.List;

import hu.kovacs.gergo.tprsapp.model.Entry;

/**
 * This class contains logging methods which help with debugging.
 *
 * @author Gergo Kovacs
 * @version 1.0
 * @date 2019.03.05.
 */


public class DebugLoggingHelper {

    /**
     *  The simple name of the class. Used for log.*() calls.
     */
    private static final String APP = DebugLoggingHelper.class.getSimpleName();

    /**
     * Logs entries to console.
     *
     * @param entries the entries
     */
    public static void logEntriesToConsole(List<Entry> entries){
        for(Entry entry : entries){
            Log.i(APP, entry.toString());
        }
    }

    /**
     * Logs entries from a file to the console.
     *
     * @param idOfStringResource the id of string resource (eg.: R.string.my_resource)
     */
    public static void logFileToConsole(final int idOfStringResource){

        List<Entry> entries = CSVReader.loadAndParseFile(idOfStringResource);

        for (Entry entry : entries) {
            Log.d(APP, "logFileToConsole: " + entry.toString());
        }
    }
}
