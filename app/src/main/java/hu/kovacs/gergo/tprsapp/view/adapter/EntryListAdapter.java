package hu.kovacs.gergo.tprsapp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hu.kovacs.gergo.tprsapp.R;
import hu.kovacs.gergo.tprsapp.model.Entry;

/**
 * The adapter for EntryViewModel
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.02.27.
 */

public class EntryListAdapter extends RecyclerView.Adapter<EntryListAdapter.EntryViewHolder> {

    /**
     * ViewHolder for the adapter
     */
    class EntryViewHolder extends RecyclerView.ViewHolder {
        // The views that are in individual list views
        private final TextView mEntryTextEnglish;
        private final TextView mEntryTextOriginal;

        private EntryViewHolder(View itemView) {
            super(itemView);
            mEntryTextEnglish = itemView.findViewById(R.id.entry_text_english);
            mEntryTextOriginal = itemView.findViewById(R.id.entry_text_original);
        }
    }

    /**
     * Used to inflate the layouts
     */
    private final LayoutInflater mInflater;

    /**
     * Cached copy of the entries
     */
    private List<Entry> mEntries;

    /**
     * Instantiates a new Entry list adapter.
     *
     * @param context the context
     */
    public EntryListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @NonNull
    @Override
    public EntryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.all_entries_item, parent, false);
        return new EntryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EntryViewHolder holder, int position) {
        if (mEntries != null) {
            // if the data is ready, it loads it into the views
            Entry current = mEntries.get(position);
            holder.mEntryTextEnglish.setText(current.getMEnglish());
            holder.mEntryTextOriginal.setText(current.getMOriginal());

        } else {
            // Covers the case of data not being ready yet.
            holder.mEntryTextEnglish.setText("No Word");
            holder.mEntryTextOriginal.setText("No Word");

        }
    }

    /**
     * Set entries.
     *
     * @param entries the entries
     */
    public void setEntries(List<Entry> entries) {
        mEntries = entries;
        notifyDataSetChanged();
    }

    /**
     * Returns the size of the entries. If the entries have not been created yet, it returns 0.
     *
     * @return the count of the items
     */
    @Override
    public int getItemCount() {
        if (mEntries != null) {
            return mEntries.size();
        } else {
            return 0;
        }
    }
}
