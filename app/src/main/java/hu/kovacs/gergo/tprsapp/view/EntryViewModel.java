package hu.kovacs.gergo.tprsapp.view;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import hu.kovacs.gergo.tprsapp.Repository;
import hu.kovacs.gergo.tprsapp.model.Entry;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The ViewMode for the entries
 *
 * @author Kovacs Gergo
 * @version 1.0
 * @date 2019.02.27.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EntryViewModel extends AndroidViewModel {
    /**
     * The List of entries which are updated by the LiveData component.
     */
    private LiveData<List<Entry>> mAllEntries;

    /**
     *  The repository that is resposible for providing access to the data.
     */
    private Repository mRepository;

    /**
     * Instantiates a new EntryViewModel.
     *
     * @param application the application
     */
    public EntryViewModel(@NonNull Application application) {
        super(application);
        mRepository = new Repository(application);
        mAllEntries = mRepository.getAllWords();
    }

    /**
     * Returns all the entries in a synchronized, LiveData<> type which ensures that whenever there
     * is a change in one or more of the entries, the list will be updated to the live data.
     *
     * @return the synchronized data
     */
    public LiveData<List<Entry>> getAllEntries() {
        return mAllEntries;
    }
}
