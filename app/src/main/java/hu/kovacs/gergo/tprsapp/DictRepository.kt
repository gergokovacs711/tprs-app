package hu.kovacs.gergo.tprsapp

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import hu.kovacs.gergo.tprsapp.database.JMDictDatabase
import hu.kovacs.gergo.tprsapp.model.dict.DictionaryEntry

/**
 * The DictRepository provides access to data used by the application.
 *
 * @author Gergo Kovacs
 *
 * @version 1.0
 *
 * @date 2019.04.30.
 */

object DictRepository {
    /**
     * Dao implementation of the database
     */
    private val jmdictDao = JMDictDatabase.instance.jmDictDao()

    /**
     *  Backing property for all the entries in the database
     */
    private val _dictionaryEntries: LiveData<PagedList<DictionaryEntry>>
            by lazy {
                val dataSourceFactory = jmdictDao.getAllEntriesPager()
                val config = PagedList.Config.Builder().setPageSize(50).build()
                LivePagedListBuilder(dataSourceFactory, config).build()
            }

    /**
     * The exposed entry list that can be accessed
     */
    val dictionaryEntries
        get() = _dictionaryEntries
}

