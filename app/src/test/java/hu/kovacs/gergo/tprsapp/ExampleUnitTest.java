package hu.kovacs.gergo.tprsapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import hu.kovacs.gergo.tprsapp.model.Entry;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    /**
     * The Mock entry.
     */
    @Mock
    Entry mockEntry;

    private static final String TEST_STRING = "english word";

    /**
     * First mock test isok.
     */
    @Test
    public void firstMockTestISOK() {
        Mockito.when(mockEntry.getMEnglish()).thenReturn(TEST_STRING);

        String result = mockEntry.getMEnglish();

        assertThat(result, is(TEST_STRING));
    }
}